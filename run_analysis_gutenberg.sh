#!/bin/bash

# Developed by Davor Davidovic
# Date: July 6th, 2017
# Affiliation: Rudjer Boskovic Institute, Zagreb, Croatia
# Project: EU H2020 Engaging the EGI Community towards an Open Science Commons (EGI-Engage)

workingDir="testAnalysis"

# ====== PREPARE WORKING ENVIRONMENT ====== #

# Download source code (davordavidovic-textanalysis project from bitbucket)
wget https://bitbucket.org/davordavidovic/textanalysis/get/master.zip -O ${workingDir}.zip
unzip ${workingDir}.zip

# Rename generic source code folder
mv davordavidovic-textanalysis-* ${workingDir}

# Position in the working folder
cd ${workingDir}

# Download virtualenv program locally
wget https://bitbucket.org/techtonik/locally/raw/tip/06.get-virtualenv.py

# Start new virtual environment
python 06.get-virtualenv.py ${workingDir}
source ${workingDir}/bin/activate


# ====== EXECUTABLE PART ====== #

# Input variables
name=$1
surname=$2
lang=$3

if [ -z "$lang" ]
then
	lang="english"
fi

# List folder contents
#ls -l

#echo "Virtualenv bin folder:"
#ls -l ${workingDir}/bin

regex="1s|.*|#!./${workingDir}/bin/python|"
sed -i ${regex} ${workingDir}/bin/pip
#cat ${workingDir}/bin/pip

# Install dependencies
echo "Installing dependencies..."

# Update pip
pip install --upgrade pip

regex="1s|.*|#!./${workingDir}/bin/python|"
sed -i ${regex} ${workingDir}/bin/pip

# Install requirements
pip install -r requirements.txt
#pip list
#cd ../../
echo "Dependencies installed..."

# Run analysis on Gutenberg project collection
python Gutenberg.py ${name} ${surname} --lang ${lang}

# Close python virtual environment
deactivate

# Compress the outputs
tar -cf gutenberg.tar gutenberg
mv gutenberg.tar ../

cd ..


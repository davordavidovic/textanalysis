#!/usr/bin/python

import sys
import argparse
import os
from os.path import isfile
from time import gmtime, strftime
import nltk, re, pprint
from nltk import word_tokenize

"""Script that gets all the texts from the Project Gutenberg
 - https://www.gutenberg.com based on the input paramters and 
 performs text analysis on these texts"""

URL = "https://www.ibiblio.org/pub/docs/books/gutenberg/"
GUTINDEX = "GUTINDEX.ALL"

def main():
    """Main function for the script."""

    """READ COMMAND LINE PARAMETERS - name and surname of the author"""
    parser = argparse.ArgumentParser(description='Reads name and surname of the author from the Gutenberg project')
    parser.add_argument("name", help="Given name of the author")
    parser.add_argument("surname", help="Surname of the author")
    parser.add_argument("--lang", help="Language in which books are written", default="english")

    args = parser.parse_args()

    downloadBookDir = 'gutenberg/'+args.name+'_'+args.surname+'/'+args.lang.lower()

    """Check if download folder already exists, if not create one, otherwise notify
    that the existing analysis results/outputs will be overwritten"""
    if os.path.exists(downloadBookDir):
        print '\n'
        print "The collections from author {0} {1} already exist!\n".format(args.name, args.surname)

        """Get the list of books (bookid + title) from the existing folder"""
        book_list = books_list(downloadBookDir)

        print "Books by author {0} {1} found in {2}\n".format(args.name, args.surname, os.path.abspath(downloadBookDir))

    else:
        """First, get the list of ebooks by author from Gutenberg project"""
        bookIds = get_ids(args.name, args.surname)

        """Otherwise, download books from Gutenberg project and return list of books (bookId + title)"""
        book_list = download_books_and_list(bookIds, downloadBookDir, args.lang)

        print '\n'
        print "Books downloaded from Gutenberg project by author {0} {1}".format(args.name, args.surname)

    for book_name in book_list.keys():
        print book_name + ' (' + book_list[book_name].split('/')[-1].split('.')[0] + ')'


    nltk.data.path.append("./nltk_data")
    """Download nltk corposa and predefined packages"""
    print "Downloading nltk data..."
    nltk.download(info_or_id="book", download_dir="./nltk_data")
    print "Downloading nltk data finished successfully"

    """Create new folder for collecting statistics"""
    output_folder = os.path.abspath(downloadBookDir) +  '/' + 'analyis-' + str(strftime('%H%M%S', gmtime()))
    os.makedirs(output_folder)

    """Make some statistics for each book"""
    for book_name in book_list.keys():
        print "Analyzing book {0}\n\n".format(book_name)
        book_path = book_list[book_name]
        
        f = open(book_path, 'r')
        raw_text = f.read()
        f.close()

        base_name = output_folder + '/' + book_name.replace(' ', '_')
        raw_file = base_name + '.txt'
        stat_file = base_name + '_stat.txt'
        f_raw = open(raw_file, 'w')
        f_stat = open(stat_file, 'w')

        """Remove header and footer"""
        raw_text = trim_raw_text(raw_text)

        """Write raw text in the fole without gutenberg's header/footer"""
        f_raw.write(raw_text)
        f_raw.close()

        tokens = word_tokenize(raw_text)
        text = nltk.Text(tokens)

        num_tokens = len(text)
        vocabulary_size = len(set(text))
        vocabulary = sorted(set(text))
        lex_richness = float(vocabulary_size) / num_tokens

        f_stat.write('Number of tokens: {0}\n'.format(num_tokens))
        f_stat.write('Number of distinct tokens: {0}\n'.format(vocabulary_size))
        f_stat.write('Percentage of lexical richness (distinct tokens/tokens): {0}%\n'.format(lex_richness*100))

        """Compute token frequencies"""
        word_count = dict()
        for word in vocabulary:
            word_count[word] = text.count(word)
        write_word_freq(word_count, base_name)

        """The most words longer than 7 letters and occured more than 7 times"""
        length = 7
        occurance = 10
        longest_words = most_common_words(text, length, occurance)
        f_stat.write('List of the most common words (length > {0} and frequency > {1})\n'.format(length, occurance))
        f_stat.write("\n".join(longest_words))
        f_stat.write("\n\n")

        # text.dispersion_plot(longest_words[:5])

        """Close output file upon finish"""
        f_stat.close()

    pass


def get_ids(name, surname):
    """Read bookIds from the main index"""

    """Download GUTENBERG INDEX file"""
    os.system('wget '+URL+GUTINDEX)

    """ Get book ids in GUTINDEX of all records by the author "Name Surname"""
    with open(GUTINDEX, 'r') as f:
        content = f.readlines()

    f.close()
    os.remove(GUTINDEX)
  
    author = 'by '+name+' '+surname
    audio = "Audio:"

    """Get only those lines that haveby Name Surname and not having Audio: """
    content = [x for x in content if author in x and audio not in x]

    """Remove blank lines from beginning/end of the lines"""
    content = [x.strip() for x in content]

    """Extract book ids (last word in line)"""
    bookIds = [myid.split()[-1] for myid in content]

    return bookIds

def download_books_and_list(bookIds, folder_path, language):

    """Download books based on the ids"""
    """Only book in English are stored"""
    remote_path = URL

    """Create download folder"""
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    folder_abs_path = os.path.abspath(folder_path)

    book_list = dict()
    """Download all books in bookIds and remove those not in english"""
    for book_id in bookIds:
        path = remote_path
        for i in list(book_id)[:-1]:
            path += i+'/'
        path += book_id

        file_name = book_id + '.txt'
        os.system('wget ' + path+'/' + book_id + '.txt' + ' -O ' + folder_abs_path + '/' + file_name)

        """Check whether downloaded files is empty, if yes, try to download other book_id-{0-9}.txt"""
        i = 9
        while os.path.getsize(folder_abs_path+'/'+file_name) == 0 and i >= 0:
            """Download other file with name book_id-i.txt"""
            os.system('wget ' + path+'/' + book_id + '-' + str(i) + '.txt' + ' -O ' + folder_abs_path + '/' + file_name)
            i -= 1

        """If book is not in English, delete it"""
        if check_language(folder_abs_path+'/'+file_name, language) == True:

            """Add title and book_id to the hash table"""
            # print book_id + ': Title of the book: ' + get_title(folder_abs_path+'/'+file_name) + '\n'
            title = get_title(folder_abs_path+'/'+file_name)
            # new_file_name = title.replace(' ', '_') + '.txt'
            # os.rename(folder_abs_path+'/'+file_name, folder_abs_path+'/'+new_file_name)

            # book_list[title] = folder_abs_path+'/'+new_file_name
            book_list[title] = folder_abs_path+'/'+file_name
  
    """Return hash table"""
    return book_list

def books_list(folder_path):
    """Generate list of books based on the title and bookId"""
    folder_abs_path = os.path.abspath(folder_path)

    book_list = dict()

    onlyfiles = [f for f in os.listdir(folder_abs_path) if isfile(os.path.join(folder_abs_path,f))]

    # for ebooks in folder_path
    for file_name in onlyfiles:
        # file_name = book_id + '.txt'
        title = get_title(folder_abs_path+'/'+file_name)
        book_list[title] = folder_abs_path+'/'+file_name

    return book_list

def check_language(file_path, language):
    """Check if the book is in the given language, if not, remove it"""
    """Returns 'True' if book is written in language specified by 'language' parameter,
    otherwise return 'False'"""

    """Open file"""
    f = open(file_path, 'r')

    not_deleted = True
    """Pass throught the lines since "Language" is found. If language is not given 'language' remove the file"""
    for line in f:
        if "Language:" in line:
            if line.split()[-1].lower() != language.lower():
                os.remove(file_path)
                #print "Removing file " + file_path
                not_deleted = False
                break

    f.close()

    return not_deleted

def get_title(file_path):
    """Return the title of the book"""

    """Open file"""
    f = open(file_path, 'r')

    """Pass throught the lines since "Title" is found. If language is different from variable 'language' remove the file"""

    """Read first 50 lines"""
    head = [next(f) for x in xrange(50)]
    head_list = ''.join(head)
    titleStart = head_list.find("Title:")+6
    titleEnd = head_list.find("Author:")-1
    title = ' '.join(head_list[titleStart:titleEnd].split())
    
    #for line in f:
        # if "Title:" in line:
            # title = line[line.index(':')+1:].strip()
            # break

    f.close()

    return title

def trim_raw_text(raw_text):
    """Remove header and footer from the text"""
    clean_text = raw_text[raw_text.find('START OF')-4:raw_text.rfind('END OF')-4]
    clean_text = clean_text[clean_text.rfind('***')+3:]

    return clean_text

def write_word_freq(word_count, base_file_name):

    """Creates a files based on book_name and write down the word frequencies in sorted order"""
    file_name = base_file_name + '_freq.txt'

    f = open(file_name, 'w')
    for w in sorted(word_count, key=word_count.get, reverse=True):
        f.write(w + ' ' + str(word_count[w])+'\n')

    f.close()

def most_common_words(text, length=0, occur=0):
    """Returns a list of most commont words with number of letter > length and occurance > occur"""

    fdist = nltk.FreqDist(text)
    long_words = sorted(w for w in set(text) if len(w) > length and fdist[w] > occur)

    return long_words

if __name__ == '__main__':
    sys.exit(main())
